import matplotlib.pyplot as plt
import time

# Fibonacci sequence
def fibonacci(n):
    fib = [0, 1] + [0]*(n-1)
    for i in range(2,n+1):
        fib[i] = fib[i-1] + fib[i-2]
    return fib

def timed_fibonacci(n):
    start = time.time()
    fib = fibonacci(n)
    return time.time()-start

x = range(0,100)
y = [timed_fibonacci(n) for n in x]

plt.plot(x,y, label='runtime')
plt.xlabel('n')
plt.legend()
plt.show()


