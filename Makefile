TARGET=main

all:
	pdflatex $(TARGET)
	pdflatex $(TARGET)
	bibtex $(TARGET)
	pdflatex $(TARGET)

clean:
	rm -f $(TARGET).bbl  $(TARGET).log  $(TARGET).pdf $(TARGET).aux  $(TARGET).blg  $(TARGET).out $(TARGET).nav $(TARGET).vrb $(TARGET).snm $(TARGET).toc
