\documentclass[a4paper,12pt]{memoir}

\usepackage{lipsum}
% package for generating dummy text -- remove for actual thesis

\usepackage{amsmath,amssymb,amsthm} % standard AMS LaTeX packages

\usepackage{listings} % for typesetting programming code and similar elements
\lstset{
  columns=flexible,
  basicstyle={\small\ttfamily},
}

\usepackage{lu-thesis}
% load after amsmath!
% requires the memoir document class as well as the following packages:
% ebgaramond, ebgaramond-maths, graphicx, hyperref, newtxmath, xparse
% load with 'times' option to use a Times New Roman replacement as default font

\usepackage{xcolor} % access to more colours
\definecolor{lundblue}{RGB}{0,0,128}
\definecolor{lundbronze}{RGB}{156,97,20}

\usepackage[colorlinks]{hyperref}
% package for generating hyperlinks
% load after all other packages to avoid compatibility issues (unless a package
% complains and say you should do otherwise)
\hypersetup{%
  colorlinks=true,%
  linkcolor=lundblue,%
  urlcolor=lundblue,%
  citecolor=lundbronze,%
}

%%%% BEGIN example usage of amsthm package

\numberwithin{equation}{section} % preface equation number with section number

% equations, theorems, propositions, lemmas and corollaries share the same counter
\theoremstyle{theorem}
\newtheorem*{utheorem}{Theorem} % unnumbered Theorem environment
\newtheorem{theorem}[equation]{Theorem} 
\newtheorem{proposition}[equation]{Proposition}
\newtheorem{lemma}[equation]{Lemma}
\newtheorem{corollary}[equation]{Corollary}

\theoremstyle{definition} % these environments are not typeset in italics, but
                          % the environment's name is typeset in boldface
\newtheorem*{udefinition}{Definition} % unnumbered Definition environment
\newtheorem{definition}[equation]{Definition}

\theoremstyle{remark} % these environments are not typeset in italics, and
                      % environment's name is typeset in italics instead of boldface
\newtheorem{remark}[equation]{Remark}
\newtheorem{example}[equation]{Example}

%%%% END example usage of amsthm package

\begin{document}

%%%%% BEGIN cover page

\author{Eve Examplesson}
% the thesis' author

\title{Guidelines and Recommendations for\\ Bachelor's and Master's Theses\\ in Mathematics}
% the thesis' title

\date{\today}
% the thesis' hand-in date

\maketitleLU{Bachelor's}{Advisor's Name} % comment for master's thesis
% \maketitleLU[Master's][Advisor's name] % uncomment for master's thesis

%%%%% END cover page

\frontmatter % pages within the front matter are numbered using lowercase Roman numerals

%%%%% BEGIN abstract

\thispagestyle{empty}

\begin{abstract}
  The thesis should include an abstract that summarises its contents;
  mathematical jargon can be utilised here. The typical length of an abstract is
  between 100 and 300 words.
\end{abstract}

%%%%% END Abstract

%%%%% BEGIN popsci description

\chapter*{Popular science description}
\addcontentsline{toc}{chapter}{Popular science description}

The thesis should include a popular science description accessible to a general
audience of non-mathematicians; in particular, mathematical jargon and overly
technical statements should be avoided in this part of the thesis. The typical
length of the popular science description is between one and two pages.

%%%%% END popsci description

%%%%% BEGIN toc

\cleardoublepage

\settocdepth{subsection}
\tableofcontents*

%%%%% END toc

%%%%% BEGIN intro

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

The thesis should include an introduction that provides an overview not only of
the contents of the thesis but also of the mathematical context in which the
thesis' topic is situated. The typical length of the introduction is about 10\%
of the total length of the main body of the thesis.

%%%%% END intro

%%%%% BEGIN main body of the thesis

\mainmatter % pages within the main matter are numbered using Arabic numerals

\chapter{First chapter}

\section{Basic \LaTeX\ usage}

\subsection{Environments and cross-referencing}

As a general rule and to facilitate cross-referencing, all definitions,
theorems, propositions, lemmas, corollaries, examples, remarks, etc., should be
declared in a separate numbered environment. These are handled by the
\texttt{amsthm} package (the package's documentation is available at the
following URL: \url{http://www.ams.org/arc/tex/amscls/amsthdoc.pdf}). Here is a
list of environment declarations to get you started.

\begin{latexcode}
\theoremstyle{theorem}
\newtheorem*{utheorem}{Theorem} % unnumbered Theorem environment
\newtheorem{theorem}[equation]{Theorem} 
\newtheorem{proposition}[equation]{Proposition}
\newtheorem{lemma}[equation]{Lemma}
\newtheorem{corollary}[equation]{Corollary}

\theoremstyle{definition}
\newtheorem*{mydef}{Definition} % unnumbered Definition environment
\newtheorem{definition}[equation]{definition}

\theoremstyle{remark}
\newtheorem{remark}[equation]{Remark}
\newtheorem{example}[equation]{Example}
\end{latexcode}

For example, placing the following code in the main body of the document

\begin{latexcode}
\begin{theorem}[Euclid's Theorem]
  \label{thm:Euclids_Theorem}
  The are infinitely many distinct primes.
\end{theorem}
\end{latexcode}

will result in the following:

\begin{theorem}[Euclid's Theorem]
  \label{thm:Euclids_Theorem}
  There are infinitely many primes.
\end{theorem}

\LaTeX\ is designed to handle cross-references for you. This means that instead
of typing \verb!by Theorem~1.1.1! (that is, typing the number 1.1.1 by yourself)
you can insetad type
\verb!by Theorem~\ref{thm:Euclids_Theorem}! and \LaTeX\ will automagically insert
the correct theorem number: by Theorem~\ref{thm:Euclids_Theorem} \ldots This is
an extremely useful feature that will save you time and prevent you from
cross-referencing mistakes (especially if you choose memorable labels). Notice
also that, thanks to the use of the \verb!hyperref! package, all
cross-references and citations contain a hyperlink. Adventurous users might want
to replace the \verb!amsthm! package by the more powerful package
\verb!cleveref!.

\begin{utheorem}
  Example of a unnumbered theorem.
\end{utheorem}

\begin{lemma}
  Example of a lemma.
\end{lemma}

\begin{proposition}
  Example of a proposition.
\end{proposition}

\begin{corollary}
  Example of a corollary.
\end{corollary}

\begin{udefinition}
  Example of a definition.
\end{udefinition}

\begin{definition}
  Example of an unnumbered definition.
\end{definition}

\begin{remark}
  Example of a remark.
\end{remark}

\begin{example}
  Example of an example.
\end{example}


\subsection{Citations}

You should \textbf{explain everything in your own words} and give precise
references to the sources you utilise. Here are some examples.

`In this section we follow closely the treatment in
\verb!\cite[Sec.~X]{A99}!.'

`We follow the proof of \verb!\cite[Prop.~Y]{B98}!.'

`We follow the main idea of proof of \verb!\cite[Prop.~Y]{B98}!, but provide
additional details for the benefit of the reader.'

You should also mention when a proof was obtained independently, for example
be prefacing your proof by the sentence `This proof is due to the author.'

For consistency and in order to avoid mistakes, you are advised to fetch the
BibTeX data for all references you use from
MathSciNet (\url{https://mathscinet.ams.org/mathscinet}) for published articles and books
(this is a subscription-based service that can only be accessed from within LU's
network) and arXiv2bibtex (\url{https://arxiv2bibtex.org/}) for arXiv preprints.

Here is an example of BibTeX data fetched from MathSciNet and is included in the
file \verb!bibliography.bib!:

\begin{latexcode}
  @book {MR2102219,
    AUTHOR = {Bourbaki, Nicolas},
    TITLE = {Theory of sets},
    SERIES = {Elements of Mathematics (Berlin)},
    NOTE = {Reprint of the 1968 English translation [Hermann, Paris;
      MR0237342]},
    PUBLISHER = {Springer-Verlag, Berlin},
    YEAR = {2004},
    PAGES = {viii+414},
    ISBN = {3-540-22525-0},
    MRCLASS = {01A75},
    MRNUMBER = {2102219},
    DOI = {10.1007/978-3-642-59309-3},
    URL = {https://doi.org/10.1007/978-3-642-59309-3},
  }
\end{latexcode}

The above reference can by cited using the specified key \verb!\cite{MR2102219}!
-- this will result in~\cite{MR2102219}. Of course, you may change the key to a
more memorable one. You are also advised to utilise a reference management
system such as JabRef (\url{https://www.jabref.org/}) or Zotero
(\url{https://www.zotero.org/}) if your bibliography consits of more than a few
entries.

\subsection{Code snippets}

To visualize code snippets, in particular Python code, in \LaTeX\ documents we recommend to use the
\pyth{listings} package. However, we have defined our own color scheme which
your can modify in the file \pyth{lu-thesis.sty}. 

It provides simple environments to include
code snippets, or sections from files or a simple line of code in a convenient
way. For example, to include an entire file you simply write 
\verb!\inputpython{fibonacci.py}[Fibonacci]!. Setting the title to
\textbf{Fibonacci} is optional.

\inputpython{fibonacci.py}[Fibonacci code]

It is also possible to only input an important section of a file, for example,
and algorithm or such. For that you can add the optional first and last file to
be included like that: \verb!\inputpython{fibonacci.py}[Fibonacci sequence][4][9]!.
This only includes lines 4 to 9 from the given file.

\inputpython{fibonacci.py}[Fibonacci sequence][4][9]

\vskip 5pt

For other small code snippets one can also use the \verb!python! environment 
\begin{latexcode}
\begin{python}[title={A function.}]
# a function 
def f(x):
  return x*x

y = f(2)
\end{python}
\end{latexcode}

which yields 

\begin{python}[title={A function.},numbers=none]
# a function 
def f(x):
  return x*x

y = f(2)
\end{python}

\vskip 5pt

You can still use the original macros from the listings package. 

\begin{latexcode}
\begin{lstlisting}[language=python]
# a function 
def f(x):
  return x*x

y = f(2)
\end{lstlisting}
\end{latexcode}

which again yields 

\begin{lstlisting}[language=python]
# a function 
def f(x):
  return x*x

y = f(2)
\end{lstlisting}

For further options reading the display of code in \LaTeX\ please read the
documentation of the listings package \cite{listings}.



\chapter{Example of a chapter}

\lipsum[1-2]

\section{Example of a section}

\lipsum[1-5]

\section{Another example section}

\lipsum[1-5]

%%%%% END main body of the thesis

%%%%% BEGIN appendices

\appendix % appendices are numbered using uppercase Roman letters

\chapter{Installing \LaTeX}

\section{Windows}

\section{MacOS}

\section{Linux}

\chapter{Example of an appendix}

\lipsum[1-15]

%%%%% END appendices

\backmatter

%%%%% BEGIN references

\bibliographystyle{alpha}
\bibliography{bibliography}

%%%%% END references

\end{document}
